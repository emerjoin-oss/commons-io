package org.emerjoin.commons.io;

public abstract class FileEntryException extends FileRepositoryException {

    private String name;

    public FileEntryException(String message, String name){
        super(message);
        this.name = name;
    }

    public FileEntryException(String message, String name, Throwable cause) {
        super(message,cause);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
