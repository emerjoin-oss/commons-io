package org.emerjoin.commons.io;

public class FileRepositoryException extends CommonsIOException {

    public FileRepositoryException(String message) {
        super(message);
    }

    public FileRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

}
