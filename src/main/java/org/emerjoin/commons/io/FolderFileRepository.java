package org.emerjoin.commons.io;

import org.emerjoin.commons.io.content.Content;
import org.emerjoin.commons.io.content.TextContent;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class FolderFileRepository implements ModifiableFileRepository {

    private File file;
    private FileFilter fileFilter;

    public FolderFileRepository(File file){
        this(file,null);
    }

    public FolderFileRepository(File file, final FileFilter fileFilter){
        if(file==null||!file.exists()||file.isFile())
            throw new IllegalArgumentException("file must be an existing directory");
        this.file = file;
        this.fileFilter = (it) -> {
            if(!it.isFile())
                return false;
            if(fileFilter!=null)
                return fileFilter.accept(it);
            return true;
        };
    }

    private void checkName(String name){
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
    }

    @Override
    public FileEntry addEmptyEntry(String name) {
        return this.addEmptyEntry(name,false);
    }

    @Override
    public FileEntry addEmptyEntry(String name, boolean overwrite) throws FileRepositoryException {
        this.checkName(name);
        File newFile = this.fileNamed(name);
        if(newFile.exists()&&!overwrite)
            throw new DuplicateFileEntryException(name);
        else if(newFile.exists()&&overwrite) {
            if(!newFile.delete())
                throw new CommonsIOException("error deleting existing file: "+file.getAbsolutePath());
        }
        try {
            if (!newFile.createNewFile())
                throw new FileRepositoryException("failure creating new file: " + newFile.getAbsolutePath());
            return new FolderFileEntry(newFile, this);
        }catch (IOException ex){
            throw new FileRepositoryException("error creating new file: "+newFile.getAbsolutePath(),
                    ex);
        }
    }

    private File fileNamed(String name){
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        return new File(file.getAbsolutePath()+File.separator+name);
    }

    @Override
    public FileEntry addEntry(String name, Content content) {
        this.checkName(name);
        if(content==null)
            throw new IllegalArgumentException("content must not be null nor empty");
        File file = fileNamed(name);
        if(file.exists())
            throw new DuplicateFileEntryException(name);
        FolderFileEntry entry = (FolderFileEntry) this.addEmptyEntry(name);
        content.writeToFile(file);
        return entry;
    }

    @Override
    public FileEntry addEntry(String name, Content content, boolean overwrite) throws FileRepositoryException {
        this.checkName(name);
        if(content==null)
            throw new IllegalArgumentException("content must not be null nor empty");
        FolderFileEntry entry = (FolderFileEntry) this.addEmptyEntry(name,overwrite);
        content.writeToFile(entry.getFile()).execute();
        return entry;
    }

    private void filePutContent(File file, Content content){
        if(file == null || !file.exists() || file.isDirectory())
            throw new IllegalArgumentException("an existing file is required");
        if(content==null)
            throw new IllegalArgumentException("content must not be null");
        try {
            FileOutputStream fout = new FileOutputStream(file);
            if (content instanceof TextContent) {
                TextContent textContent = (TextContent) content;
                PrintStream ps = new PrintStream( fout, true, textContent.charset().name());
                ps.write(textContent.bytes());
            } else fout.write(content.bytes());
        }catch (IOException ex){
            throw new FileRepositoryException(
                    String.format("error putting %s content into file: %s",content.getClass().getSimpleName(),
                        file.getAbsolutePath()),
                            ex);
        }
    }

    private FolderFileEntry asFileBackedFileEntry(FileEntry entry){
        if(entry==null)
            throw new IllegalArgumentException("entry must not be null");
        if(!(entry instanceof FolderFileEntry))
            throw new IllegalArgumentException("entry must be of FolderFileEntry type");
        return (FolderFileEntry) entry;
    }

    @Override
    public FileEntry copy(FileEntry entry, String name) {
        this.checkName(name);
        return this.copy(entry,name,false);
    }

    @Override
    public FileEntry copy(FileEntry entry, String name, boolean overwrite) throws FileRepositoryException {
        this.checkName(name);
        if(entry==null)
            throw new IllegalArgumentException("entry must not be null");
        if(exists(name)&&!overwrite)
            throw new DuplicateFileEntryException(name);
        else if(exists(name) && overwrite)
            this.deleteByName(name);
        return this.addEntry(name,entry.getContent().asBinary(),
                overwrite);
    }

    @Override
    public void deleteEntry(FileEntry entry) throws FileRepositoryException {
        if(entry==null)
            throw new IllegalArgumentException("entry must not be null");
        FolderFileEntry fileEntry = asFileBackedFileEntry(entry);
        fileEntry.deleteFile();
    }

    @Override
    public void deleteIfExists(String name) throws FileRepositoryException {
        if(this.exists(name)){
            this.deleteByName(name);
        }
    }

    private void deleteByName(String name){
        FolderFileEntry.deleteFile(fileNamed(
                name));
    }

    @Override
    public boolean isEmpty() {
        return count() == 0;
    }

    @Override
    public Stream<FileEntry> entries() {
        return toStream(file.listFiles(fileFilter));
    }

    private Stream<FileEntry> toStream(File[] files){
        if(files==null)
            return Stream.empty();
        List<FileEntry> folderFileEntries = new ArrayList<>();
        for(File file: files){
            folderFileEntries.add(new FolderFileEntry(file, this));
        }
        return folderFileEntries.stream();
    }


    @Override
    public long count() {
        if(fileFilter!=null)
            return  lengthOf(file.listFiles(fileFilter));
        return lengthOf(file.listFiles());
    }

    private long lengthOf(File[] files){
        if(files==null)
            return 0;
        return files.length;
    }

    @Override
    public boolean exists(FileEntry entry) {
        FolderFileEntry fileEntry = asFileBackedFileEntry(entry);
        return fileEntry.getFile().exists();
    }

    @Override
    public boolean exists(String entryName) {
        this.checkName(entryName);
        File file = this.fileNamed(entryName);
        return file.exists();
    }


    public Optional<FileEntry> getEntryByName(String name) throws FileRepositoryException {
        File file =  fileNamed(name);
        if(!file.exists()){
            return Optional.empty();
        }

        FolderFileEntry fileBackedFileEntry = new FolderFileEntry(file, this);
        return Optional.of(fileBackedFileEntry);
    }

    @Override
    public void waitForNewEntries() throws FileRepositoryException, InterruptedException {

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

}
