package org.emerjoin.commons.io;

public class FileEntryNotFoundException extends FileEntryException {

    public FileEntryNotFoundException(String name) {
        super("File entry not found: "+name, name);
    }

}
