package org.emerjoin.commons.io;

public class DuplicateFileEntryException extends FileEntryException {

    public DuplicateFileEntryException(String name) {
        super("There is already a File entry with name="+name,name);
    }

}
