package org.emerjoin.commons.io;

import org.emerjoin.commons.io.content.BinaryContent;
import org.emerjoin.commons.io.content.TextContent;

import java.nio.charset.Charset;

public class DefaultFileEntryContent implements FileEntryContent {

    private ContentProvider provider;
    private TextContent cachedTextContent;
    private BinaryContent cachedBinaryContent;

    public DefaultFileEntryContent(ContentProvider provider){
        if(provider==null)
            throw new IllegalArgumentException("provider must not be null");
        this.provider = provider;
    }

    @Override
    public TextContent asText() {
        return asText(true);
    }

    @Override
    public TextContent asText(boolean cacheInstance){

        return asText(cacheInstance,Charset.defaultCharset());

    }

    @Override
    public TextContent asText(boolean cacheInstance, Charset charset){
        if(charset==null)
            throw new IllegalArgumentException("charset must not be null");
        if(cacheInstance && cachedTextContent ==null){
            return cachedTextContent = new TextContent(provider,
                    charset);
        }else if(cacheInstance){
            return cachedTextContent;
        }else return new TextContent(provider,
                charset);
    }

    @Override
    public BinaryContent asBinary() {
        return asBinary(true);
    }

    @Override
    public BinaryContent asBinary(boolean cacheInstance){
        if(cacheInstance && cachedBinaryContent ==null)
            return cachedBinaryContent = new BinaryContent(provider);
        else if(cacheInstance)
            return cachedBinaryContent;
        else return new BinaryContent(provider);
    }

}
