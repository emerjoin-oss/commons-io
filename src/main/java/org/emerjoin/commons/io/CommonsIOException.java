package org.emerjoin.commons.io;

public class CommonsIOException extends RuntimeException {

    public CommonsIOException(String message){
        super(message);
    }

    public CommonsIOException(String message, Throwable cause){
        super(message,cause);
    }

}
