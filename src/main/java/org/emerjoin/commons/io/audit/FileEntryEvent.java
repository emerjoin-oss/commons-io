package org.emerjoin.commons.io.audit;

public class FileEntryEvent {

    private String entryName;
    private FileEventType eventType;

    public FileEntryEvent(String entryName,FileEventType eventType){
        if(entryName==null||entryName.isEmpty())
            throw new IllegalArgumentException("entryName must not be null nor empty");
        if(eventType==null)
            throw new IllegalArgumentException("eventType must not be null");
        this.entryName = entryName;
        this.eventType = eventType;
    }

    public String getEntryName() {
        return entryName;
    }

    public FileEventType getEventType() {
        return eventType;
    }
}
