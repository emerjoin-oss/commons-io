package org.emerjoin.commons.io.audit;

public enum FileEventType {
    DELETED, CREATED, CONTENT_UPDATED
}
