package org.emerjoin.commons.io.audit;

import org.emerjoin.commons.io.ModifiableFileRepository;
import java.util.stream.Stream;

public interface AuditableFileRepository extends ModifiableFileRepository {

    Stream<FileEntryEvent> getEventsHistory();

}
