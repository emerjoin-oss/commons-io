package org.emerjoin.commons.io;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface FileRepository {

    boolean isEmpty() throws FileRepositoryException;
    Stream<FileEntry> entries() throws FileRepositoryException;
    long count() throws FileRepositoryException;
    boolean exists(FileEntry entry) throws FileRepositoryException;
    boolean exists(String entryName) throws FileRepositoryException;
    Optional<FileEntry> getEntryByName(String name) throws FileRepositoryException;
    void waitForNewEntries() throws FileRepositoryException, InterruptedException;

}
