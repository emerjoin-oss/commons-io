package org.emerjoin.commons.io;

import java.io.InputStream;

@FunctionalInterface
public interface ContentProvider {

    InputStream provide();

}
