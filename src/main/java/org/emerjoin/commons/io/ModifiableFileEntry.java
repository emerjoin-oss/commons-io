package org.emerjoin.commons.io;

import org.emerjoin.commons.io.content.Content;

import java.io.OutputStream;

public interface ModifiableFileEntry extends FileEntry {

    OutputStream writeStream(WriteMode mode);
    void delete() throws FileRepositoryException;
    FileEntry rename(String name) throws FileRepositoryException;
    FileEntry rename(String name, boolean overwrite) throws FileRepositoryException;
    void changeContent(Content content) throws FileRepositoryException;

}
