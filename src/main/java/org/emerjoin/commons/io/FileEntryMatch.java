package org.emerjoin.commons.io;

@FunctionalInterface
public interface FileEntryMatch {

    boolean match(FileEntry entry);

}
