package org.emerjoin.commons.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class Buffered {

    public static InputStream input(InputStream inputStream){
        return input(inputStream, CommonsIOConfig.current().
                getInputBufferSize());
    }

    public static InputStream input(InputStream inputStream, int bufferSize){
        if(inputStream==null)
            throw new IllegalArgumentException("inputStream must not be null");
        checkBufferSize(bufferSize);
        if(bufferSize==0)
            return inputStream;
        return new BufferedInputStream(inputStream,bufferSize);
    }

    public static OutputStream output(OutputStream outputStream){
        return output(outputStream, CommonsIOConfig.current().
                getOutputBufferSize());
    }

    public static OutputStream output(OutputStream outputStream, int bufferSize){
        if(outputStream==null)
            throw new IllegalArgumentException("outputStream must not be null");
        checkBufferSize(bufferSize);
        if(bufferSize==0)
            return outputStream;
        return new BufferedOutputStream(outputStream,
                bufferSize);
    }

    private static void checkBufferSize(int bufferSize){
        if(bufferSize<0)
            throw new IllegalArgumentException("buffer size must not be less than zero");
    }

}
