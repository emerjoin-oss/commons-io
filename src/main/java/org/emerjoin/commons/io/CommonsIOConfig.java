package org.emerjoin.commons.io;

public class CommonsIOConfig {

    private int inputBufferSize = 1024;
    private int outputBufferSize = 1024;

    private static CommonsIOConfig CURRENT;

    public static CommonsIOConfig current(){
        if(CURRENT==null)
            return CURRENT = new CommonsIOConfig();
        return CURRENT;
    }

    public int getInputBufferSize() {
        return inputBufferSize;
    }

    public void setInputBufferSize(int inputBufferSize) {
        if(inputBufferSize<0)
            throw new IllegalArgumentException("inputBufferSize must not be less than zero");
        this.inputBufferSize = inputBufferSize;
    }

    public int getOutputBufferSize() {
        return outputBufferSize;
    }

    public void setOutputBufferSize(int outputBufferSize) {
        if(outputBufferSize<0)
            throw new IllegalArgumentException("outputBufferSize must not be less than zero");
        this.outputBufferSize = outputBufferSize;
    }

    public void apply(){
        CURRENT = this;
    }

}
