package org.emerjoin.commons.io;

import org.emerjoin.commons.io.content.Content;

public interface ModifiableFileRepository extends FileRepository {

    FileEntry addEmptyEntry(String name) throws FileRepositoryException;
    FileEntry addEmptyEntry(String name, boolean overwrite) throws FileRepositoryException;
    FileEntry addEntry(String name, Content content) throws FileRepositoryException;
    FileEntry addEntry(String name, Content content, boolean overwrite) throws FileRepositoryException;
    FileEntry copy(FileEntry entry, String name) throws FileRepositoryException;
    FileEntry copy(FileEntry entry, String name, boolean overwrite) throws FileRepositoryException;
    void deleteEntry(FileEntry entry) throws FileRepositoryException;
    void deleteIfExists(String name) throws FileRepositoryException;

}
