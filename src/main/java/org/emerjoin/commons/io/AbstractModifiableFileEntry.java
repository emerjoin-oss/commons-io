package org.emerjoin.commons.io;

public abstract class AbstractModifiableFileEntry implements ModifiableFileEntry {

    private String name;
    private ModifiableFileRepository repository;

    public AbstractModifiableFileEntry(String name, ModifiableFileRepository fileRepository){
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        if(fileRepository==null)
            throw new IllegalArgumentException("fileRepository must not be null");
        this.name = name;
        this.repository = fileRepository;
    }

    protected ModifiableFileRepository getRepository(){
        return this.repository;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void delete() throws FileRepositoryException {
        if(!repository.exists(this))
            throw new FileEntryNotFoundException(getName());
        this.repository.deleteEntry(this);
    }

    @Override
    public FileEntry rename(String name) throws FileRepositoryException {
        return this.rename(name,false);
    }

    @Override
    public FileEntry rename(String name, boolean overwrite) throws FileRepositoryException {
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        if(this.repository.exists(name)&&!overwrite)
            throw new DuplicateFileEntryException(name);
        FileEntry newEntry = this.repository.addEntry(name, getContent().asBinary(false),overwrite);
        this.repository.deleteEntry(this);
        return newEntry;
    }

}
