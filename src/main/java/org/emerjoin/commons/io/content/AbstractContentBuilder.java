package org.emerjoin.commons.io.content;

import org.emerjoin.commons.io.Buffered;
import org.emerjoin.commons.io.CommonsIOConfig;
import org.emerjoin.commons.io.ContentProvider;

import java.io.InputStream;

public abstract class AbstractContentBuilder<T extends Content> {

    private int bufferSize;
    private ContentProvider provider;
    private InputStream sourceStream;

    public AbstractContentBuilder(ContentProvider contentProvider){
        this.provider = contentProvider;
        this.setDefaultBufferSize();
    }

    public AbstractContentBuilder(InputStream source){
        this.sourceStream = source;
        this.setDefaultBufferSize();
    }

    private void setDefaultBufferSize(){
        this.bufferSize = CommonsIOConfig.current().getInputBufferSize();
    }

    AbstractContentBuilder bufferSize(int size){
        if(size<0)
            throw new IllegalArgumentException("buffer size must not be less than 0");
        this.bufferSize = size;
        return this;
    }

    protected int getBufferSize(){
        return this.bufferSize;
    }

    private InputStream source(){
        if(sourceStream!=null)
            return sourceStream;
        else return provider.provide();
    }

    protected InputStream getBufferedStream(){
        return Buffered.input(source(),
                bufferSize);
    }

    public boolean isProviderSource(){
        return provider != null;
    }

    public abstract T build();

}
