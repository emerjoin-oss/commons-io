package org.emerjoin.commons.io.content;

import org.emerjoin.commons.io.CommonsIOException;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ContentDigest {

    private Content content;

    ContentDigest(Content content){
        this.content = content;
    }

    public String sha1(){
        return this.digestContent("SHA-1");
    }

    public String md5(){
        return this.digestContent("MD5");
    }

    private String digestContent(String algorithm){
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            byte[] hashInBytes = md.digest(content.bytes());
            return DatatypeConverter.printHexBinary(hashInBytes);
        }catch (NoSuchAlgorithmException ex){
            throw new CommonsIOException("Digest algorithm not found: "+algorithm,
                    ex);
        }
    }

}
