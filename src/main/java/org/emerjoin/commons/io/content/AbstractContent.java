package org.emerjoin.commons.io.content;

import org.emerjoin.commons.io.CommonsIOException;
import org.emerjoin.commons.io.ContentProvider;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.util.Base64;

public abstract class AbstractContent extends Content {

    private ContentProvider provider;
    private InputStream sourceStream;
    private boolean initialized = false;
    private boolean refreshable = false;

    public AbstractContent(){
        setInitialized();
    }


    protected void setInitialized(){
        this.initialized = true;
    }

    public AbstractContent(ContentProvider provider){
        if(provider==null)
            throw new IllegalArgumentException("provider must not be null");
        this.provider = provider;
        this.refreshable = true;
    }

    public AbstractContent(InputStream sourceStream){
        if(sourceStream==null)
            throw new IllegalArgumentException("sourceStream must not be null");
        this.sourceStream = sourceStream;
        this.refreshable = false;
    }


    public InputStream stream(){
        return new ByteArrayInputStream(bytes());
    }

    public DataCopier writeToFile(File file){
        return writeToFile(file,false);
    }

    public DataCopier writeToFile(File file, boolean append){
        if(file==null||!file.exists()||file.isDirectory())
            throw new IllegalArgumentException("file must not be null and must exist");
        try {
            FileOutputStream outputStream = new FileOutputStream(file,append);
            return copyData(outputStream);
        }catch (IOException ex){
            throw new CommonsIOException("error accessing file: "+file.getAbsolutePath(),
                    ex);
        }
    }

    public DataCopier copyData(OutputStream outputStream){
        if(outputStream==null)
            throw new IllegalArgumentException("outputStream must not be null");
        return new DataCopier(stream(),
                outputStream);
    }


    protected boolean hasProvider(){

        return this.provider != null;

    }

    protected boolean initialized(){
        return this.initialized;
    }

    protected InputStream getSource(){
        if(provider!=null)
            return this.provider.provide();
        return this.sourceStream;
    }


    protected byte[] loadToByteArray(){
        InputStream source = getSource();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        DataCopier copier = new DataCopier(source,bout);
        copier.execute();
        return bout.toByteArray();
    }

    protected void mustBeInitialized(){
        if(!initialized())
            this.initialize();
    }

    protected void initialize(){
        byte[] data = loadToByteArray();
        this.load(data);
        this.initialized = true;
    }

    abstract protected void load(byte[] data);

    public  boolean isRefreshable(){
        return this.refreshable;
    }

    public void refresh(){
        if(!isRefreshable())
            throw new IllegalStateException("content not refreshable");
        this.initialize();
    }

    public ContentDigest digest(){
        return new ContentDigest(this);
    }

    public String hexEncode(){
        return DatatypeConverter.printHexBinary(
                bytes());
    }

    public String base64Encode(){
        return Base64.getEncoder().encodeToString(
                bytes());
    }

}
