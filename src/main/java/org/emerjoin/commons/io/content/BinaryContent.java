package org.emerjoin.commons.io.content;

import org.emerjoin.commons.io.CommonsIOException;
import org.emerjoin.commons.io.ContentProvider;
import org.emerjoin.commons.io.IOUtils;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.util.Base64;

public class BinaryContent extends AbstractContent {

    private byte[] bytes;

    public BinaryContent(byte[] bytes){
        if(bytes==null)
            throw new IllegalArgumentException("bytes array must not be null");
        this.bytes = bytes;
    }

    public BinaryContent(ContentProvider provider){
        super(provider);
    }

    public byte[] bytes(){
        mustBeInitialized();
        return this.bytes;
    }

    @Override
    public long length() {
        mustBeInitialized();
        return this.bytes.length;
    }

    @Override
    public boolean isEmpty() {
        mustBeInitialized();
        return this.bytes.length==0;
    }

    public static Builder fromFile(File file){
        if(file==null||!file.exists()||file.isDirectory())
            throw new IllegalArgumentException("file must not be null and must exist");
        return new BinaryContent.Builder(() -> {
            try {
                return new FileInputStream(file);
            } catch (FileNotFoundException ex) {
                throw new CommonsIOException("failed to access file: "+file.getAbsolutePath(),
                        ex);
            }
        });
    }


    public static Builder fromInput(InputStream stream){
        if(stream==null)
            throw new IllegalArgumentException("stream must not be null");
        return new Builder(stream);
    }


    @Override
    protected void load(byte[] data) {
        this.bytes = data;
    }

    public static class Builder extends AbstractContentBuilder<BinaryContent> {

        private Builder(InputStream inputStream){
            super(inputStream);
        }

        private Builder(ContentProvider provider){
            super(provider);
        }

        @Override
        public BinaryContent build() {
            if(isProviderSource()){
                return new BinaryContent(() -> getBufferedStream());
            }
            return new BinaryContent(IOUtils.toByteArray(
                    getBufferedStream()));
        }

    }

}
