package org.emerjoin.commons.io.content;

import org.emerjoin.commons.io.CommonsIOException;

import java.io.*;

public class DataCopier {

    private InputStream input;
    private OutputStream output;

    private int inputBufferSize = 0;
    private int outputBufferSize = 0;

    public DataCopier(InputStream input, OutputStream output){
        if(input==null)
            throw new IllegalArgumentException("input must not be null");
        if(output==null)
            throw new IllegalArgumentException("output must not be null");
        this.input = input;
        this.output = output;
    }

    public void execute(){
        InputStream inputStream = input;
        OutputStream outputStream = output;
        if(inputBufferSize>0)
            inputStream = new BufferedInputStream(inputStream,inputBufferSize);
        if(outputBufferSize>0)
            outputStream = new BufferedOutputStream(outputStream,outputBufferSize);
        try {
            int lastByte = inputStream.read();
            while (lastByte != -1) {
                outputStream.write(lastByte);
                lastByte = inputStream.read();
            }
            outputStream.flush();
        }catch (IOException ex){
            throw new CommonsIOException("error copying data",
                    ex);
        }finally {
            try {
                outputStream.close();
            } catch (IOException ex) {
                throw new CommonsIOException("error closing output stream",ex);
            }
        }
    }

    public DataCopier inputBuffer(int size){
        this.checkBufferSize(size);
        this.inputBufferSize = size;
        return this;
    }

    public DataCopier outputBuffer(int size){
        this.checkBufferSize(size);
        this.outputBufferSize = size;
        return this;
    }

    private void checkBufferSize(int size){
        if(size<1)
            throw new IllegalArgumentException("buffer size must be higher than zero");
    }


}
