package org.emerjoin.commons.io.content;

public class EmptyContent extends AbstractContent {

    EmptyContent(){
    }

    @Override
    public long length() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public byte[] bytes() {
        return new byte[0];
    }

    @Override
    protected void load(byte[] data) {

    }

}
