package org.emerjoin.commons.io.content;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class Content {

     protected Content(){

     }

     public abstract long length();
     public abstract boolean isEmpty();
     public abstract DataCopier writeToFile(File file);
     public abstract DataCopier writeToFile(File file, boolean append);
     public abstract byte[] bytes();
     public abstract InputStream stream();
     public abstract DataCopier copyData(OutputStream outputStream);
     public abstract boolean isRefreshable();
     public abstract void refresh();

     public abstract ContentDigest digest();
     public abstract String hexEncode();
     public abstract String base64Encode();

     public static Content empty(){
          return new EmptyContent();
     }


}
