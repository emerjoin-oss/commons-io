package org.emerjoin.commons.io.content;

import org.emerjoin.commons.io.CommonsIOException;
import org.emerjoin.commons.io.ContentProvider;
import org.emerjoin.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Scanner;

public class TextContent extends AbstractContent {

    private String text;
    private Charset charset;

    public TextContent(String text){
        this(text,Charset.defaultCharset());
    }

    public TextContent(String text,  Charset charset){
        if(text==null)
            throw new IllegalArgumentException("text must not be null");
        if(charset==null)
            throw new IllegalArgumentException("charset must not be null");
        this.text = text;
        this.charset = charset;
    }

    public TextContent(ContentProvider provider){
        this(provider,Charset.defaultCharset());
    }

    public TextContent(ContentProvider provider, Charset charset){
        super(provider);
        if(charset==null)
            throw new IllegalArgumentException("charset must not be null");
        this.charset = charset;
    }

    public String text(){
        this.mustBeInitialized();
        return this.text;
    }

    public Charset charset(){
        return this.charset;
    }

    public long length() {
        this.mustBeInitialized();
        return this.text.length();
    }

    public boolean isEmpty() {
        this.mustBeInitialized();
        return this.text.isEmpty();
    }


    @Override
    public byte[] bytes() {
        this.mustBeInitialized();
        return text.getBytes(charset);
    }

    public Scanner scanner(){
        return new Scanner(stream(),charset.
                name());
    }

    public static TextContent fromBytes(byte[] bytes, Charset charset){
        return new TextContent(new String(bytes,
                charset));
    }

    public static Builder fromInput(InputStream inputStream, Charset charset){
        if(inputStream==null)
            throw  new IllegalArgumentException("inputStream must not be null");
        if(charset==null)
            throw new IllegalArgumentException("charset must not be null");
        return new Builder(() -> inputStream, charset);
    }

    public static Builder fromInput(InputStream inputStream){
        if(inputStream==null)
            throw new IllegalArgumentException("inputStream must not be null");
        return new Builder(inputStream,Charset.defaultCharset());
    }

    public static Builder fromFile(File file){
        return fromFile(file,Charset.defaultCharset());
    }

    public static Builder fromFile(File file, Charset charset){
        if(file==null||!file.exists()||file.isDirectory())
            throw new IllegalArgumentException("file must not be null and must exist");
        if(charset==null)
            throw new IllegalArgumentException("charset must not be null");
        return new Builder(() -> {
            try {
                return new FileInputStream(file);
            } catch (FileNotFoundException ex) {
                throw new CommonsIOException("failed to access file: "+file.getAbsolutePath(),
                        ex);
            }
        },charset);
    }

    @Override
    protected void load(byte[] data) {
        this.text = new String(data,charset);
    }


    public static class Builder extends AbstractContentBuilder<TextContent> {

        private Charset charset;

        private Builder(ContentProvider provider, Charset charset){
            super(provider);
            this.charset = charset;
        }

        private Builder(InputStream inputStream, Charset charset){
            super(inputStream);
            this.charset = charset;
        }

        @Override
        public TextContent build() {
            if(isProviderSource()){
                return new TextContent(() -> getBufferedStream(),
                        charset);
            }
            return TextContent.fromBytes(IOUtils.toByteArray(getBufferedStream()),
                    charset);
        }
    }

}
