package org.emerjoin.commons.io;

public enum WriteMode {
    Append, Overwrite
}