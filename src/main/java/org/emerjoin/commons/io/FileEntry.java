
package org.emerjoin.commons.io;

import java.io.InputStream;

public interface FileEntry {

    String getName();
    FileEntryContent getContent();
    InputStream readStream();

}
