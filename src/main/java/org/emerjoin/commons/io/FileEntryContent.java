package org.emerjoin.commons.io;

import org.emerjoin.commons.io.content.BinaryContent;
import org.emerjoin.commons.io.content.TextContent;

import java.nio.charset.Charset;

public interface FileEntryContent {

    TextContent asText();
    TextContent asText(boolean cacheInstance);
    TextContent asText(boolean cacheInstance, Charset charset);
    BinaryContent asBinary();
    BinaryContent asBinary(boolean cacheInstance);

}
