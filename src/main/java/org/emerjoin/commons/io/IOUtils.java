package org.emerjoin.commons.io;

import org.emerjoin.commons.io.content.DataCopier;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public final class IOUtils {

    public static byte[] toByteArray(InputStream inputStream){
        if(inputStream==null)
            throw new IllegalArgumentException("inputStream must not be null");
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        DataCopier dataCopier = new DataCopier(inputStream,bout);
        dataCopier.execute();
        return bout.toByteArray();
    }

}
