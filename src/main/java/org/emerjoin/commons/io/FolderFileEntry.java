package org.emerjoin.commons.io;

import org.emerjoin.commons.io.content.Content;

import java.io.*;

public class FolderFileEntry extends AbstractModifiableFileEntry {

    private File file;
    private DefaultFileEntryContent content;
    private FolderFileRepository repository;

    FolderFileEntry(File file, FolderFileRepository repository){
        super(file.getName(),repository);
        this.file = file;
        this.repository = repository;
        ContentProvider fileContentProvider = () -> {
            try {
                FileInputStream inputStream = new FileInputStream(file);
                return Buffered.input(inputStream);
            }catch (IOException ex){
                throw new CommonsIOException("error accessing file: "+file.getAbsolutePath(),
                        ex);
            }
        };
        this.content = new DefaultFileEntryContent(fileContentProvider);
    }

    protected File getFile(){
        return this.file;
    }

    private OutputStream openOutputStream(){
        try {
            return new FileOutputStream(file);
        } catch (FileNotFoundException ex) {
            throw new CommonsIOException("error creating output stream for file: "+file.getAbsolutePath(),
                    ex);
        }
    }

    @Override
    public OutputStream writeStream(WriteMode mode) {
        if(mode==null)
            throw new IllegalArgumentException("mode must not be null");
        try {
            return new FileOutputStream(file,mode== WriteMode.Append);
        }catch (IOException ex){
            throw new FileRepositoryException("error creating file output stream: "+file.getAbsolutePath(),ex);
        }
    }

    @Override
    public void changeContent(Content content) throws FileRepositoryException {
        if(content==null)
            throw new IllegalArgumentException("content must not be null");
        content.copyData(this.openOutputStream())
                .outputBuffer(CommonsIOConfig.current().getOutputBufferSize())
                .execute();
    }

    @Override
    public String getName() {
        return this.file.getName();
    }

    @Override
    public DefaultFileEntryContent getContent() {
        return this.content;
    }

    @Override
    public InputStream readStream() {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            throw new FileRepositoryException("file not found: "+file.getAbsolutePath(),
                    ex);
        }
    }


    protected static void deleteFile(File file){
        if(!file.exists())
            throw new FileRepositoryException("cant delete a file that does not exist: "+file.getAbsolutePath());
        if(!file.delete())
            throw new FileRepositoryException("error deleting file: "+file.getAbsolutePath());
    }

    protected void deleteFile(){
        deleteFile(file);
    }

}
