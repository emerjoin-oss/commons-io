package org.emerjoin.commons.io.virtual;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public abstract class ProxiedByteArrayOutputStream extends ByteArrayOutputStream {

    public ProxiedByteArrayOutputStream(){
        super();
    }

    public void close() throws IOException {
        super.close();
        this.closed();
    }

    public abstract void closed();

}
