package org.emerjoin.commons.io.virtual;

import org.emerjoin.commons.io.*;
import org.emerjoin.commons.io.content.Content;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

public class VirtualFileEntry extends AbstractModifiableFileEntry {

    private byte[] data = new byte[]{};
    private ContentProvider contentProvider;
    private FileEntryContent entryContent;

    VirtualFileEntry(String name, VirtualFileRepository fileRepository, Content content){
        super(name, fileRepository);
        this.contentProvider = () -> {
            return new ByteArrayInputStream(data);
        };
        this.entryContent = new DefaultFileEntryContent(contentProvider);
        if(content!=null)
            data = content.bytes();
    }

    VirtualFileEntry(String name, VirtualFileRepository fileRepository) {
        this(name,fileRepository,null);
    }

    private VirtualFileRepository repository(){
        return (VirtualFileRepository) getRepository();
    }

    @Override
    public OutputStream writeStream(WriteMode mode) {
        if(mode==null)
            throw new IllegalArgumentException("mode must not be null");
        return new ProxiedByteArrayOutputStream() {
            @Override
            public void closed() {
                if(mode==WriteMode.Overwrite)
                    data = toByteArray();
                else{
                    byte[] written = toByteArray();
                    byte[] buff = new byte[data.length+written.length];
                    System.arraycopy(data,0,buff,0,data.length);
                    System.arraycopy(written,0,buff,data.length,written.length);
                    data = buff;
                }
            }
        };
    }

    @Override
    public void changeContent(Content content) throws FileRepositoryException {
        if(content==null)
            throw new IllegalArgumentException("content must not be null");
        this.data = content.bytes();
        repository().changedEntryContent(this);
    }

    @Override
    public FileEntryContent getContent() {
        return this.entryContent;
    }

    @Override
    public InputStream readStream() {
        return new ByteArrayInputStream(data);
    }
}
