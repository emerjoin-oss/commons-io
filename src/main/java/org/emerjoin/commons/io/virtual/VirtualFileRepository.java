package org.emerjoin.commons.io.virtual;

import org.emerjoin.commons.io.DuplicateFileEntryException;
import org.emerjoin.commons.io.FileEntry;
import org.emerjoin.commons.io.FileEntryNotFoundException;
import org.emerjoin.commons.io.FileRepositoryException;
import org.emerjoin.commons.io.audit.AuditableFileRepository;
import org.emerjoin.commons.io.audit.FileEntryEvent;
import org.emerjoin.commons.io.audit.FileEventType;
import org.emerjoin.commons.io.content.Content;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class VirtualFileRepository implements AuditableFileRepository {

    private Map<String,FileEntry> map = new ConcurrentHashMap<>();
    private List<FileEntryEvent> entryEventList = new ArrayList<>();

    @Override
    public Stream<FileEntryEvent> getEventsHistory() {
        return entryEventList.stream();
    }

    public void deleteEventsHistory(){
        this.entryEventList.clear();
    }

    private void checkEntryName(String name){
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("entry name must not be null nor empty");
    }

    @Override
    public FileEntry addEmptyEntry(String name) throws FileRepositoryException {
        return this.addEmptyEntry(name,false);
    }

    @Override
    public FileEntry addEmptyEntry(String name, boolean overwrite) throws FileRepositoryException {
        this.checkEntryName(name);
        if(exists(name)&&!overwrite)
            throw new DuplicateFileEntryException(name);
        if(exists(name)&&overwrite)
            this.deleteEntryByName(name);
        VirtualFileEntry entry = new VirtualFileEntry(name,this);
        this.map.put(name,entry);
        this.entryEventList.add(new FileEntryEvent(name,FileEventType.CREATED));
        return entry;
    }


    private void deleteEntryByName(String name){
        this.map.remove(name);
        this.entryEventList.add(new FileEntryEvent(name, FileEventType.DELETED));
    }

    @Override
    public FileEntry addEntry(String name, Content content) throws FileRepositoryException {
        return this.addEntry(name,content,false);
    }

    @Override
    public FileEntry addEntry(String name, Content content, boolean overwrite) throws FileRepositoryException {
        this.checkEntryName(name);
        if(exists(name)&&!overwrite)
            throw new DuplicateFileEntryException(name);
        else if(exists(name)&&overwrite)
            this.deleteEntryByName(name);
        VirtualFileEntry entry = new VirtualFileEntry(name,this,content);
        this.map.put(name,entry);
        this.entryEventList.add(new FileEntryEvent(name,FileEventType.CREATED));
        return entry;
    }

    @Override
    public FileEntry copy(FileEntry entry, String name) throws FileRepositoryException {
        return this.copy(entry,name,false);
    }

    @Override
    public FileEntry copy(FileEntry entry, String name, boolean overwrite) throws FileRepositoryException {
        checkEntryName(name);
        if(entry==null)
            throw new IllegalArgumentException("entry must not be null");
        if(exists(name)&&!overwrite)
            throw new DuplicateFileEntryException(name);
        else if(exists(name)&&overwrite)
            this.deleteEntryByName(name);
        return this.addEntry(name,entry.getContent().asBinary());
    }

    @Override
    public void deleteEntry(FileEntry entry) throws FileRepositoryException {
        if(entry==null)
            throw new IllegalArgumentException("entry must not be null");
        if(!exists(entry))
            throw new FileEntryNotFoundException(entry.getName());
        this.deleteEntryByName(entry.getName());
    }

    @Override
    public void deleteIfExists(String name) throws FileRepositoryException {
        if(this.exists(name))
            this.deleteEntryByName(name);
    }


    protected void changedEntryContent(FileEntry entry) throws FileRepositoryException {
        this.entryEventList.add(new FileEntryEvent(entry.getName(),FileEventType.
                CONTENT_UPDATED));
    }

    @Override
    public boolean isEmpty() throws FileRepositoryException {
        return map.isEmpty();
    }

    @Override
    public Stream<FileEntry> entries() throws FileRepositoryException {
        return Collections.unmodifiableMap(map).values()
                .stream();
    }

    @Override
    public long count() throws FileRepositoryException {
        return map.size();
    }

    @Override
    public boolean exists(FileEntry entry) throws FileRepositoryException {
        if(entry==null)
            throw new IllegalArgumentException("entry must not be null");
        return map.values().contains(entry);
    }

    @Override
    public boolean exists(String entryName) throws FileRepositoryException {
        this.checkEntryName(entryName);
        return this.map.containsKey(entryName);
    }

    @Override
    public Optional<FileEntry> getEntryByName(String name) throws FileRepositoryException {
        this.checkEntryName(name);
        return Optional.ofNullable(this.map.get(
                name));
    }

    @Override
    public void waitForNewEntries() throws FileRepositoryException, InterruptedException {

        //TODO: Implement
        throw new UnsupportedOperationException();

    }
}
