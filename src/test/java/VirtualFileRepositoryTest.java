import org.emerjoin.commons.io.DuplicateFileEntryException;
import org.emerjoin.commons.io.FileEntry;
import org.emerjoin.commons.io.ModifiableFileEntry;
import org.emerjoin.commons.io.audit.FileEntryEvent;
import org.emerjoin.commons.io.audit.FileEventType;
import org.emerjoin.commons.io.content.Content;
import org.emerjoin.commons.io.content.TextContent;
import org.emerjoin.commons.io.virtual.VirtualFileRepository;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertTrue;

public class VirtualFileRepositoryTest {

    @Test
    public void addEntry_must_add_an_event_to_the_history(){
        VirtualFileRepository repository = new VirtualFileRepository();
        String entryName = "example.txt";
        FileEntry entry = repository.addEntry(entryName, Content.empty());
        Optional<FileEntryEvent> entryEvent  = repository.getEventsHistory()
                .filter(it -> it.getEntryName().equals(entryName) && it.getEventType() == FileEventType.CREATED)
                .findAny();
        assertTrue(entryEvent.isPresent());
    }

    @Test(expected = DuplicateFileEntryException.class)
    public void addEntry_without_overwrite_must_fail_if_entry_already_exists(){
        VirtualFileRepository repository = new VirtualFileRepository();
        String entryName = "example.txt";
        repository.addEntry(entryName, Content.empty());
        repository.addEntry(entryName, Content.empty());
    }

    @Test
    public void addEntry_with_overwrite_must_succeed_when_entry_already_exists(){
        VirtualFileRepository repository = new VirtualFileRepository();
        String entryName = "example.txt";
        repository.addEntry(entryName, Content.empty());
        repository.addEntry(entryName, Content.empty(),true);
    }

    @Test
    public void rename_entry_must_add_two_events_to_the_repository_history(){
        VirtualFileRepository repository = new VirtualFileRepository();
        String initialFilename = "example1.txt";
        String finalFilename = "example2.txt";
        ModifiableFileEntry entry  = (ModifiableFileEntry) repository.addEntry(initialFilename,
                Content.empty());
        entry.rename(finalFilename);
        Optional<FileEntryEvent> deleteEntryEvent  = repository.getEventsHistory()
                .filter(it -> it.getEntryName().equals(initialFilename) && it.getEventType() == FileEventType.DELETED)
                .findAny();
        Optional<FileEntryEvent> createEntryEvent  = repository.getEventsHistory()
                .filter(it -> it.getEntryName().equals(finalFilename) && it.getEventType() == FileEventType.CREATED)
                .findAny();
        assertTrue(deleteEntryEvent.isPresent());
        assertTrue(createEntryEvent.isPresent());
    }

    @Test
    public void delete_entry_from_stream_must_not_fail(){
        VirtualFileRepository repository = new VirtualFileRepository();
        String initialFilename = "example1.txt";
        String finalFilename = "example2.txt";
        repository.addEntry(initialFilename, Content.empty());
        repository.addEntry(finalFilename, Content.empty());
        repository.entries().forEach(it -> ((ModifiableFileEntry) it).delete());
    }


    @Test(expected = DuplicateFileEntryException.class)
    public void copy_entry_without_overwrite_must_fail_if_file_already_exists(){
        String filename1 = "foo.txt";
        String filename2 = "foo.bar.txt";
        VirtualFileRepository repository = new VirtualFileRepository();
        FileEntry entry = repository.addEntry(filename1, new TextContent("This is a simple content"));
        repository.addEntry(filename2,Content.empty());
        repository.copy(entry,filename2);
    }

    @Test
    public void copy_entry_with_overwrite_must_succeed_when_file_already_exists(){
        String filename1 = "foo.txt";
        String filename2 = "foo.bar.txt";
        VirtualFileRepository repository = new VirtualFileRepository();
        FileEntry entry = repository.addEntry(filename1, new TextContent("This is a simple content"));
        repository.addEntry(filename2,Content.empty());
        repository.copy(entry,filename2,true);
    }

    @Test
    public void copy_entry_must_add_an_entry_to_the_repository_history(){
        String filename1 = "foo.txt";
        String filename2 = "foo.bar.txt";
        VirtualFileRepository repository = new VirtualFileRepository();
        FileEntry entry = repository.addEntry(filename1, new TextContent("This is just a simple content"));
        repository.copy(entry,filename2);
        Optional<FileEntryEvent> entryEvent = repository.getEventsHistory()
                .filter(it -> it.getEntryName().equals(filename2) && it.getEventType() == FileEventType.CREATED)
                .findAny();
        assertTrue(entryEvent.isPresent());
    }

    @Test
    public void copy_entry_must_copy_the_entry_content_without_compromising_it(){
        String filename1 = "foo.txt";
        String filename2 = "foo.bar.txt";
        VirtualFileRepository repository = new VirtualFileRepository();
        FileEntry firstEntry = repository.addEntry(filename1, new TextContent("This is just a simple content"));
        FileEntry secondEntry = repository.copy(firstEntry,filename2);
        String firstEntryDigest = firstEntry.getContent().asBinary().digest().md5();
        assertTrue(firstEntryDigest.equals(secondEntry.getContent().asBinary().digest().md5()));
    }

}
