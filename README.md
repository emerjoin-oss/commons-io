# Maven Coordinates
## Repository
Emerjoin Snapshots repository
```xml
<repository>
    <id>emerjoin-oss</id>
    <name>Emerjoin-snapshots</name>
    <url>https://pkg.emerjoin.org/artifactory/oss-snapshot</url>
    <snapshots>
         <enabled>true</enabled>
         <updatePolicy>always</updatePolicy>
    </snapshots>
</repository>
```

## Artifact
Latest version
```xml
<dependency>
    <groupId>org.emerjoin.commons</groupId>
    <artifactId>commons-io</artifactId>
    <version>0.2.0-beta-SNAPSHOT</version>
</dependency>
```

# Why?
We built this library mainly to two Concepts: File and Directory. This was crucial to us in order to ease Testing of Code that Manipulated files in a single directory.

# Features
We went a bit beyond our initial motivation. We ended up doing this:
* Abstraction of File and Folder Concepts
* Easy Manipulation of file content
* Buffering to speed file content reading and writing
* File Operations Audit Capability
* Easy file integrity verification using MD5 and SHA-1

# Concept
This library is built around three core Concepts:
* FileRepository - some sort of repository of File entries
* FileEntry - represents a file record
* Content - represents data that can be put into and retrieved from a FileEntry.


# Examples
## Using a Folder as a FileRepository
The simplest thing you can do is use a Folder a FileRepository as shown below:
```java
    File folder = new File("path/to/some/where");
    ModifiableFileRepository repository = new FolderFileRepository(folder);
```

## Using a Virtual FileRepository
The **FileRepository** keeps all **FileEntry** in memory and can be audited. It is ideal for Unit Testing.
```java
    ModifiableFileRepository repository = new VirtualFileRepository();
```

## File Entry operations
In this section will find examples regarding File Entry operations

### Adding a Text File to a Repository
```java
    String filename = "my_filename.txt";
    FileEntry entry = repository.addEntry(filename,new TextContent(
            "This is just an example"));
```

### Adding a Binary File to a Repository
```java
    byte[] myData = //whatever
    String filename = "my_filename.txt";
    FileEntry entry = repository.addEntry(filename,new BinaryContent(myData));
```

### Changing the content of a File Entry
```java
    repository.getEntryByName("my_filename.txt").ifPresent(it -> {
        ModifiableFileEntry entry = (ModifiableFileEntry) it;
        entry.changeContent(new TextContent("My new Text Content"));
    })
```

### Renaming a File Entry
```java
    String filename = "my_filename.txt";
    ModifiableFileEntry entry (ModifiableFileEntry) = repository.addEntry(...);
    FileEntry newEntry = entry.rename("new_text_file.txt");
```

### Checking the Integrity of a File Entry using MD5
```java
    String expectedDigest = //whatever
    FileEntry myEntry = //whatever
    String digest = myEntry.getContent().asBinary().digest().md5();
    assert digest.equals(expectedDigest)
```

### Checking the Integrity of a File Entry using SHA-1
```java
    String expectedDigest = //whatever
    FileEntry myEntry = //whatever
    String digest = myEntry.getContent().asBinary().digest().sha1();
    assert digest.equals(expectedDigest)
```

## Other examples

### Auditing a File Repository
You can only audit an **AuditableFileRepository**:

#### Checking if a File Entry was created
```java
    AuditableFileRepository repository = //whatever
    Stream<FileEntryEvent> events = repository.getEventsHistory();
    Optional<FileEntryEvent> entryEvent = events
            .filter(it -> it.getEntryName().equals("example.txt") && it.getEventType() == FileEventType.CREATED)
            .findAny();
    assert entryEvent.isPresent();
```     

#### Checking if a File Entry was deleted
```java
    AuditableFileRepository repository = //whatever
    Stream<FileEntryEvent> events = repository.getEventsHistory();
    Optional<FileEntryEvent> entryEvent = events
            .filter(it -> it.getEntryName().equals("example.txt") && it.getEventType() == FileEventType.DELETED)
            .findAny();
    assert entryEvent.isPresent();
``` 

#### Checking if the content of a File Entry was updated
```java
    AuditableFileRepository repository = //whatever
    Stream<FileEntryEvent> events = repository.getEventsHistory();
    Optional<FileEntryEvent> entryEvent = events
            .filter(it -> it.getEntryName().equals("example.txt") && it.getEventType() == FileEventType.CONTENT_UPDATED)
            .findAny();
    assert entryEvent.isPresent();
```

### Creating TextContent from File
```java
    TextContent.fromFile(new File("path/to/my/file"))
        .bufferSize(2040) //Input buffer size (Optional)
        .build();
```

### Creating BinaryContent from File
```java
    BinaryContent.fromFile(new File("path/to/my/file"))
        .bufferSize(2040) //Input buffer size (Optional)
        .build();
```

### Encoding Content to base64
```java
    Content content = //whatever
    String encoded = content.base64Encode();
```

### Encoding Content to Hex
```java
    Content content = //whatever
    String encoded = content.hexEncode();
```

# Configurations
The default configurations of the Library are defined via the **CommonsIOConfig** object.
Example:
```java
    CommonsIOConfig config = new CommonsIOConfig();
    config.setInputBufferSize(1024);
    config.setOutputBufferSize(1024);
    config.apply();
``` 

